class CreateBooks < ActiveRecord::Migration[6.1]
  def change
    create_table :books do |t|
      t.string :theme
      t.string :edition
      t.string :issue
      t.string :kind
      t.string :url_image
      t.string :name
      t.references :author, null: false, foreign_key: true

      t.timestamps
    end
  end
end
