class Loan < ApplicationRecord
  belongs_to :book
  belongs_to :user

  enum status: {
    active: 0,
    closed: 1
  }
end
