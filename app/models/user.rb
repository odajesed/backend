class User < ApplicationRecord
    has_secure_password

    has_one :admin
    has_one :clerk
    has_one :client

    validates :name, presence: true
    validates_uniqueness_of :email

end
