class AuthenticationController < ApplicationController
  def login
    user = User.find_by(email: params[:user][:email])
    user = user&.authenticate(params[:user][:password])
    if user
      token = JsonWebToken.encode(user_id: user.id)
      render json: {token: token}
    else
      render json: {message: "Nao foi possivel fazer login", status: 401}
    end
  end

  def sign_up
    @user = User.new(user_params.except(:role))

    if @user.save
      if user_params[:role] == "client"
        client = Client.new(user_id: @user.id)
        if client.save
          render json: {user: @user, client: client}, status: :created
        else
          @user.destroy
          render json: client.errors, status: 422
        end
      elsif user_params[:role] == "admin"
        admin = Admin.new(user_id: @user.id)
        if admin.save
          render json: {user: @user, admin: admin}, status: :created
        else
          @user.destroy
          render json: admin.errors, status: 422
        end
      elsif user_params[:role] == "clerk"
        clerk = Clerk.new(user_id: @user.id)
        if clerk.save
          render json: {user: @user, clerk: clerk}, status: :created
        else
          @user.destroy
          render json: clerk.errors, status: 422
        end
      else
        @user.destroy
        render json: {message:"Nao há esse tipo", status: 422}
      end
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end
    private
      def user_params
        params.require(:user).permit(:email, :name, :password, :password_confirmation, :role, :cpf, :telephone)
      end
  end

